import checkInput from './check-input'
import CheckForm from './check-form'
import tests from './tests'

console.log('Username: ', checkInput('user', tests.username))
console.log('Username: ', checkInput('username', tests.username))
console.log('Username (regex): ', checkInput('user', tests.password))
console.log('Username (regex): ', checkInput('username', tests.password))

const formOptions = {
  el: document.getElementsByTagName('form')[0],
  tests: tests
}

const checkLogin = CheckForm.extend(formOptions)
