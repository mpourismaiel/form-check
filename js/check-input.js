/**
 * checks input using a condition funciton
 * @param  {string} input     some text to perform actions on
 * @param  {function} condition - we use this to check whether input is valid
 * or not
 * @return {boolean}           validity of input
 */

export default function checkInput (input, condition) {
  // If a test function is available on condition we can assume condition is
  // a regex and we can use the native behaviour.
  if (typeof condition.test === 'function') {
    return condition.test(input)
  } else {
    return condition(input)
  }
}
