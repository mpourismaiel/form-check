import checkInput from './check-input'

const CheckForm = {
  extend ({el, tests}) {
    const newInstance = Object.assign({}, this, options)
    newInstance.constructor()
    return newInstance
  },

  constructor () {
    this.el
      .querySelectorAll('input:not([type=submit])') // get all non button inputs
      .forEach(this.addListener.bind(this)) // add appropriate listeners to them
  },
  
  addListener : function (input) {
    // I'll bind testInput to input so the keyword **this** would refer to the
    // input. This is done in order for the function to have a good idea of
    // what data it's gonna use. I also could simply apply input as an argument
    // to the function. There is no difference here.
    input.addEventListener('keypress', this.testInput.bind(this, input))
  },
  
  testInput : function (input) {
    if (checkInput(input.value, this.tests[input.id])) {
      input.classList.remove('error')
    } else  {
      input.classList.add('error')
    }
  }
}

export default CheckForm
